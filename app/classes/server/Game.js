var fs 				= require('fs');

var EnemyFactory 	= require('./EnemyFactory.js');

module.exports =  function Game(room, map_size, wavesTemplate){

	this.status = 'uninitialized';

	this.room = room;

	this.enemyFactory = new EnemyFactory(map_size, wavesTemplate);

	this.mapSize = { w : map_size.w, h : map_size.h }

	this.waves = [];

	this.KeyToId = [];

	this.imgSrcs = {
		player	: [
		'imgs/Ship/f0.png',
		'imgs/Ship/f1.png',
		'imgs/Ship/f2.png',
		'imgs/Ship/f3.png'
		],
		enemy		: 'imgs/Enemy/Example/e_f1.png',
		background 	: 'imgs/Backgrounds/farback.gif',
		explosion	: 'imgs/explosion/explosion_transparent.png',
		shoot 		: 'imgs/Ship/shoot.png',
		bigShoot 	: 'imgs/Ship/shoot.png'

	};

	this.gameInterval = 0;

	this.WavesInterval = 0;


	this.init = function(iosocket){

		this.waves = this.enemyFactory.createLevel();

		initX = 10;

		initY = 30;

		for (var i = this.room.players.length - 1; i >= 0; i--) {

			this.KeyToId[this.room.players[i].Id] = i;

			this.room.players[i].ship.src = this.imgSrcs.player[0];

			this.room.players[i].ship.coords.X = initX;
			this.room.players[i].ship.coords.Y = initY + 20;
			initY += 40;
		};
		this.status = 'initialized';
		iosocket.in(this.room.roomName).emit('start_game', this.room);
		// remove room from roomManager
		// send update multi room event
	}


	this.getShootPosition = function(player){

		if(player.ship.coords == undefined)
			return false;
		x = player.ship.coords.X + player.ship.coords.Width + 1;
		y = player.ship.coords.Y + player.ship.coords.Width / 2;

		return {X:x,Y:y};
	}

	this.createShoot = function(player){

		shootPos = this.getShootPosition(player);
		this.room.shoots.push({
			X 		: shootPos.X,
			Y 		: shootPos.Y,
			Width	: 22,
			Height	: 9,
			Id 		: player.Id + '-' + this.uniqId(),
			Src 	: this.imgSrcs.shoot,
			speed 	:{
				Left	: false,
				Up 		: false,
				Right	: true,
				Down	: false
			}
		});
	}

	this.createBigShoot = function(player){

		shootPos = this.getShootPosition(player);
		// creation de l'objet tir(objet json en dur, pas de manager)
		this.room.shoots.push({
			X 		: shootPos.X,
			Y 		: shootPos.Y,
			Width	: 20,
			Height	: 8,
			Id 		: player.Id + '-' + this.uniqId(),
			Src 	: this.imgSrcs.bigShoot,
			speed 	: {
				Left	: false,
				Up 		: false,
				Right	: true,
				Down	: false
			}
		});
	}

	this.spawnWave = function(){
		this.room.waves.push(this.waves.shift());
	}

	this.applySpeed = function(coords, speed, acceleration){

		ax = (acceleration != undefined ? acceleration.X : 3 );
		ay = (acceleration != undefined ? acceleration.Y : 3 );

		/*if((speed.Left || speed.Right) && (speed.Down || speed.Up))
		{
			ax -= ax * 2/3;
			ay -= ay * 2/3;
		}*/

		X = (speed.Left ? coords.X - ax : coords.X);
		X = (speed.Right ? coords.X + ax : X);

		Y = (speed.Down ? coords.Y + ay : coords.Y);
		Y = (speed.Up ? coords.Y - ay : Y);

	// prevent map outgoing

	/*// if enemy ship, remove enemy from waves array
	X = ( X < this.mapSize.w && X > 0 ? X : coords.X );
	Y = ( Y < this.mapSize.h && Y > 0 ? Y : coords.Y );
	*/
	return    {X:X,Y:Y};

}

this.collide = function (obj1, obj2){

	return !(
		(obj1.X > obj2.X + obj2.Width) ||
		(obj1.X +obj1.Width < obj2.X) || 
		(obj1.Y > obj2.Y + obj2.Height) || 
		(obj1.Y + obj1.Height < obj2.Y)
		);

}

this.updateFrame = function(){

	this.room.collisionElements = [];
	// checker les collisions player <--> enemy
	for (var i = this.room.players.length - 1 ; i >= 0 ; i--) {

		old_coords = {
			X :this.room.players[i].ship.coords.X,
			Y:this.room.players[i].ship.coords.Y
		};

		new_coords = this.applySpeed(old_coords,this.room.players[i].ship.speed, {X : 6, Y :6});
		
		X = new_coords.X; 
		Y = new_coords.Y; 
		
		// if enemy ship, remove enemy from waves array
		X = ( (X < this.mapSize.w && X > 0) || (X > this.mapSize.w) ? X : old_coords.X );
		Y = ( Y < this.mapSize.h && Y > 0 ? Y : old_coords.Y );
		
		this.room.players[i].ship.coords.X = X;
		this.room.players[i].ship.coords.Y = Y;
	};

	/****************************

	check collision tirs


	****************************/
	if(this.room.shoots.length > 0){
		for (var k = this.room.shoots.length - 1; k >= 0; k--) {
			oldcoords 		= {X : this.room.shoots[k].X, Y : this.room.shoots[k].Y};
			shoot_coords 	= this.applySpeed(oldcoords, this.room.shoots[k].speed, {X : 30 ,Y : 30});

			if(shoot_coords.X > this.mapSize.w || shoot_coords.X < 0){ // colide X wall
				this.room.collisionElements.push({
					objA : this.room.shoots[k],
					objB :'WALL',
				});
				this.room.shoots.splice(k, 1);
			} else {
				this.room.shoots[k].X = shoot_coords.X;
				this.room.shoots[k].Y = shoot_coords.Y;
			}
		}
	}

	if(this.room.waves != undefined && this.room.waves.length > 0) {

		for (var i = this.room.waves.length - 1; i >= 0; i--) {
			for (var j = this.room.waves[i].length - 1; j >= 0; j--) {
							// 1 enemy ship
							old_coords = {
								X : this.room.waves[i][j].X,
								Y : this.room.waves[i][j].Y
							};
							curr_speed = this.room.waves[i][j].path.shift();
							this.room.waves[i][j].path.push(curr_speed);

							newcoords = this.applySpeed(old_coords, curr_speed);

							// check des collisions wall <--> enemy
							// si collision, detruire l'enemy + ajouter colision de la frame

							if(newcoords.X > this.mapSize.w || newcoords.X < 0){ // colide X wall

								this.room.collisionElements.push({
									objA : this.room.waves[i][j],
									objB :'WALL',
								});
								this.room.waves[i].splice(j, 1);
								if(this.room.waves[i].length < 1)
									this.room.waves.splice(i, 1);
								break;
							}
				/***************************************

				collision enemy <--> tir

				***************************************/
				breaked = false;
				if(this.room.shoots.length > 0 && this.room.shoots.length !== undefined) {
					for (var k = this.room.shoots.length - 1; k >= 0; k--) {

						if(this.collide(this.room.shoots[k],this.room.waves[i][j])){

							breaked = true;
							// enlever le tir, l'enemy, et les push dans this.room.collisionElements			
							this.room.collisionElements.push({
								objA : this.room.shoots[k],
								objB : this.room.waves[i][j],
							});
							this.room.shoots.splice(k, 1);
							this.room.waves[i].splice(j, 1);
							// if the wave is empty remove from waves[]
							if(this.room.waves[i].length < 1)
								this.room.waves.splice(i, 1);
							break;
						}
						/*else {
							shoot_oldcoords 		= {X : this.room.shoots[k].X, Y : this.room.shoots[k].Y};
							shoot_coords 			= this.applySpeed(shoot_oldcoords, this.room.shoots[k].speed, {X : 15 ,Y : 15});
							this.room.shoots[k].X 	= shoot_coords.X;
							this.room.shoots[k].Y 	= shoot_coords.Y;
						}*/
					};
				}
				if(breaked !== undefined && breaked) break;
				if(this.collide(this.room.players[0].ship.coords, this.room.waves[i][j])) {
					//this.room.players[0].ship.life--;
					this.room.collisionElements.push({
						objA : this.room.players[0],
						objB : this.room.waves[i][j]
					});
					// remove enemy from wave
					//this.room.players.splice(0, 1);
					this.status = 'loose';

					this.room.waves[i].splice(j, 1);
					// if the wave is empty remove from waves[]
					if(this.room.waves[i].length < 1)
						this.room.waves.splice(i, 1);
					break;
				}
				if(this.room.players[1] != undefined && this.collide(this.room.players[1].ship.coords, this.room.waves[i][j])){
					//this.room.players[1].ship.life--;
					this.room.collisionElements.push({
						objA : this.room.players[1],
						objB : this.room.waves[i][j]
					});
					//this.room.players.splice(1, 1);
					this.status = 'loose';

					// remove enemy from wave
					this.room.waves[i].splice(j, 1);
					// if the wave is empty remove from waves[]
					if(this.room.waves[i].length < 1)
						this.room.waves.splice(i, 1);
					break;
				}
				// if no collision, update enemy coords
				if(this.room.waves[i][j] != undefined){
					this.room.waves[i][j].X = newcoords.X;
					this.room.waves[i][j].Y = newcoords.Y;
				}
			};
		};
	}
	return this.room;
}

this.run = function(iosocket){
	this.status = 'play';
	that = this;
	stopwaves = false;
	
	this.WavesInterval = setInterval(function(){
		if(that.waves != undefined && that.waves.length > 0){
			that.spawnWave();
		} else {
			if(stopwaves === true)
				that.end(iosocket,'win');
			stopwaves = true;
		}
	},10000);
	
	this.gameInterval = setInterval(function(){
		room = that.updateFrame();
		if(that.status == 'loose'){
			that.pause();
			that.end(iosocket, 'loose');
		} else {

			console.log(that.room);
			iosocket.in(that.room.roomName).emit('update_frame', room);
		}
	}, 80);
}

this.uniqId = function(){
	const inc = 1;
	return parseInt(+new Date * Math.random()) + (inc++);
}

this.pause = function(){
	this.status = 'pause';
	clearInterval(this.gameInterval);
	clearInterval(this.WavesInterval);
}

this.unpause = function(iosocket){
	this.run(iosocket);
}

this.updateKeyToId = function(idtoremove){
	for (id in this.KeyToId)	{
		if(id == idtoremove) {
			this.KeyToId.splice(id, 1);
		} else {
			this.KeyToId[id] = 0;
		}
	}
}

this.end = function(iosockets, winorloose){
	if(winorloose === undefined){
		status = "undefined";
	} else if(winorloose == 'win') {
		status = "win";
	} else if (winorloose == 'loose'){
		status = "loose";
	}
	iosockets.in(this.room.roomName).emit('end-game', status);
}

	// End class Game
}

