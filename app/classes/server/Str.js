module.exports = function Str(socket){

	this.esc = function(str){
		if(typeof str !== 'string')
			return false;
		return str.trim().split(' ').join('_');
	}

	this.unesc = function(str){
		if(typeof str !== 'string')
			return false;
		return str.trim().split('_').join(' ');
	}
	// end class Str
};