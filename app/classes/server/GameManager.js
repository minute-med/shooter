var Game 		= require('./Game.js');
module.exports 	= function GameManager(socket){

	this.iosocket = socket;
	
	this.games = [];

	this.nbGames = 0;


	this.createGame = function(room, map_size, wavesTemplate) {
		newgame = new Game(room, map_size, wavesTemplate);
		this.games.push(newgame);
		this.nbGames++;
		return newgame;
	}


	this.removeGame = function(roomName){
		for (var i = this.games.length - 1; i >= 0; i--) {
			if (this.games[i].room.roomName == roomName){
				this.games.splice(i, 1);
				this.nbGames--;
			}
		};
	}

	this.getGameByUserId = function(userId){

		if(this.games.length < 1)
			return false;
		for (var i = this.games.length - 1; i >= 0; i--) {

			for (var j = this.games[i].room.players.length - 1; j >= 0; j--) {
				if (this.games[i].room.players[j].Id == userId)
					return this.games[i];
			};
		};
		return false;
	};


	// End class GameManager
}