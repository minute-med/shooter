// creer un systeme de lvls ou l'on peut generer le niveau
// a partir d'un fichier json
/*
exemple

wavesType = ['standard', 'zigzag', 'mini-boss', 'boss'];

game = [
	[0,1,2,1,0,2,1,0,1,3]
	[0,1,2,1,0,2,1,0,1,3]
	[0,1,2,1,0,2,1,0,1,3]
]

*/ 

module.exports = function enemyFactory(map_size, wavesTemplate){

	this.wavesTemplate = wavesTemplate;

	this.mapSize = map_size;

	this.enemySrcs = {standard : 'imgs/Enemy/Example/e_f1.png'};

	this.getStdPath = function (){
		path = [];
		for (var i = 0; i < 4; i++) {
			path.push({
				Left:true,
				Up:false,
				Right:false,
				Down:false
			});
		};
		return (path);
	}

	this.getZigZagPath = function(){

		var path = [];
		for (var i = 40 - 1; i >= 0; i--) {
			if(i > 19)
			{
				path.push({
					Left:true,
					Up:false,
					Right:false,
					Down:true
				});
			} 
			else 
			{
				path.push({
					Left:true,
					Up:true,
					Right:false,
					Down:false
				});	
			}
		};
		return path;
	}
	this.uniqId = function(){
		const inc = 1;
		return parseInt(+new Date * Math.random()) + (inc++);
	}

	this.createWave = function (type, enemyNumber){
		wave = [];
		base_x = this.mapSize.w - 50;
		base_y = 20;

		stepy = (this.mapSize.h - 40) / enemyNumber;

		switch(type){

			case 'standard':
			path = this.getStdPath();
			break;

			case 'zigzag':
			path = this.getZigZagPath();
			break;

			case 'mini-boss':

			break;

			case 'boss':

			break;

			default:
			return false;
			break;
		}

		for (var i = enemyNumber - 1; i >= 0; i--) {
			wave.push({
				X 		: base_x,
				Y 		: base_y,
				Width	: 40,
				Height	: 40,
				Id 		: this.uniqId(),
				path  	: path,
				Src 	: this.enemySrcs.standard
			});
			base_y += stepy;
		};
		return (wave);
	}

	this.createLevel = function(nbWaves, difficulty){
		waves = [];
		for (var i = 4; i >= 0; i--) {
				waves.push(this.createWave('standard', 5));
		};
		return waves;
	}

}