var Room 		= require('./Room.js');
module.exports = function RoomManager(socket){

	this.iosocket = socket;
	
	this.rooms = [];

	this.nbRooms = 0;


	this.createRoom = function(socket, room_name, user){

		room = new Room(room_name);
		room.Owner = user.nickname;
		this.rooms.push(room);
		this.nbRooms++;

		createdroom = this.joinRoom(socket, room_name, user);

		return createdroom; 
	}

	this.removeRoomByName = function(roomName){
		for (var i = this.rooms.length - 1; i >= 0; i--) {
			if(this.rooms[i].roomName == roomName){
				this.rooms.splice(i, 1);
				return true;
			}
		};
		return false;
	}

	this.joinRoom = function(socket, room_name, user){
		for (var i = this.rooms.length - 1; i >= 0; i--) {
			if (this.rooms[i].roomName == room_name){
				this.rooms[i].addPlayer(user);
				socket.join(this.rooms[i].roomName);
				return this.rooms[i];
			}
		};
		return false;
	}	

	this.leaveRoom = function(){
		
	}

	this.getRoomByUserId = function(userId){

		if(this.rooms.length < 1)
			return false;
		for (var i = this.rooms.length - 1; i >= 0; i--) {

			for (var j = this.rooms[i].players.length - 1; j >= 0; j--) {
				if (this.rooms[i].players[j].Id == userId)
					return this.rooms[i];
			};
		};
		return false;
	};


	// End class RoomManager
}