module.exports = function UserManager(socket){

	this.iosocket = socket;

	this.users 	= [];

	this.nbUsers = 0;

	this.addUser = function(user){
		this.users.push(user);
		this.nbUsers+= 1;
	}

	this.removeUser = function(nickname){
		
	}

	this.removeUserById = function(id){
		
		for (var i = this.users.length - 1; i >= 0; i--) {
			if(this.users[i].Id == id) {
				removedUser = this.users[i];	
				this.users.splice(i, 1);
				return removedUser;
			}
		};	
		return false;
	}

	this.getUserById = function(socketId){
		for (var i = this.users.length - 1; i >= 0; i--) {
			if(this.users[i].Id == socketId) {
				return this.users[i];
			}
		};
		return false;
	}

	this.userExist = function(nickname){
		if(this.users.length < 1)
			return false;
		for (var i = this.users.length - 1; i >= 0; i--) {
			if(this.users[i].nickname == nickname)
				return true;
		};
		return false;
	}
	// End class UserManager
}