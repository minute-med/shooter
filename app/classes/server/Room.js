module.exports = function Room(name){

	this.roomName = name;

	this.players = [];

	this.shoots = [];

	this.waves = [];

	this.collisionElements = [];

	this.nbPlayers = 0;

	this.Owner = '';
	
	/*
	this.init = function(){

	}
	*/

	this.addPlayer = function(player){
		this.players.push(player);
		this.nbPlayers++;
	}

	this.removePlayer = function(playerid){
			for (var i = this.players.length - 1; i >= 0; i--) {
				if(this.players[i].Id == playerid){
					this.players.splice(i, 1);
					this.nbPlayers--;
					return true;
				}
			};
			return false;
	}
	// End class Room
}