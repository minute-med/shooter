//$(document).ready(function(){});

function gameUi()
{
	this.stage = new Kinetic.Stage({
		container: 'kontainer',
		width: 800,
		height: 600
	});

	this.bgLayer = new Kinetic.Layer();
	
	this.layer = new Kinetic.Layer();

	this.createImage = function(params){

		var imgObj = new Image();
    that = this;
    imgObj.onload = function() {

      var img = new Kinetic.Image({
        x: params.X,
        y: params.Y,
        image: imgObj,
        id:params.Id,
        width: params.Size.w,
        height: params.Size.h
      });

      that.layer.add(img);
      that.stage.add(that.layer);
    }
    imgObj.src = params.Src;
  }

  this.moveImage = function(id, x, y, layer){

    for (var i = this.layer.children.length - 1; i >= 0; i--) {
      if(this.layer.children[i].attrs.id == id)
        this.layer.children[i].setPosition(x, y);
    }
  }

  this.init = function(room){

   var bgObj = new Image();
   that = this;
   bgObj.onload = function() {

    var bg = new Kinetic.Image({
     x: 0,
     y: 0,
     image: bgObj,
     id:'bg-img',
     width: $('#kontainer').width(),
     height: $('#kontainer').height()
   });

    that.bgLayer.add(bg);
    that.stage.add(that.bgLayer);
  }

  bgObj.src = 'imgs/Backgrounds/farback.gif';

  for (var i = room.players.length - 1; i >= 0; i--) 
  {

    params = 
    { 
      X:room.players[i].ship.coords.X,
      Y:room.players[i].ship.coords.Y,
      Size:{w:60,h:30},
      Src: room.players[i].ship.src,
      layer:this.layer,
      Id: room.players[i].ship.id
    }
    this.createImage(params);
  }
}

this.destroyImage = function(img){
  for (var i = this.layer.children.length - 1; i >= 0; i--) {

    if(this.layer.children[i].attrs.id == img.Id)
      this.layer.children[i].remove();
  };
  //this.bgLayer.draw();
}

this.destroyImageById = function(id){
  for (var i = this.layer.children.length - 1; i >= 0; i--) {

    if(this.layer.children[i].attrs.id == id)
      this.layer.children[i].remove();
  };
}

this.displayWaves = function(waves){
    	/************************************
    	 parcourir les waves
    	 si un element d'un wave n'existe pas
    	 dans le layer, creer l'image,
    	 sinon, bouger sa position

      ***********************************/

      for (var i = waves.length - 1; i >= 0; i--) {
            // iteration sur le tableau de waves
            for (var j = waves[i].length - 1; j >= 0; j--) {
            // iteration sur une wave d'enemis

            found = false;

            for (var k = this.layer.children.length - 1; k >= 0; k--) {

              if (waves[i][j].Id == this.layer.children[k].attrs.id){
                    // si l'image de l'enemi est deja cree dans le layer
                    found = true;
                    break;
                  } 
                };
                if(found){
                  x = waves[i][j].X;
                  y = waves[i][j].Y;
                  this.moveImage(waves[i][j].Id, x, y, this.layer);
                } else {
                  params = {
                    X:waves[i][j].X,
                    Y:waves[i][j].Y,
                    Size:{w : waves[i][j].Width, h : waves[i][j].Width},
                    Src:waves[i][j].Src,
                    layer:this.layer,
                    Id: waves[i][j].Id
                  };
                  this.createImage(params);
                }
              };

            };
          }

          this.displayShoots = function(shoots){

            for (var i = shoots.length - 1; i >= 0; i--) {
            // iteration sur le tableau de shoots

            found = false;

            for (var k = this.layer.children.length - 1; k >= 0; k--) {

              if (shoots[i].Id == this.layer.children[k].attrs.id){
                    // si l'image du shoot est deja cree dans le layer
                    found = true;
                    break;
                  }
                };
                if(found){
                  x = shoots[i].X;
                  y = shoots[i].Y;
                  this.moveImage(shoots[i].Id, x, y, this.layer);
                } else {
                  params = {
                    X     : shoots[i].X,
                    Y     : shoots[i].Y,
                    Size  : {w : shoots[i].Width, h : shoots[i].Width},
                    Src   : shoots[i].Src,
                    layer : this.layer,
                    Id    : shoots[i].Id                    
                  };
                  this.createImage(params);
                }
              };
            }

            this.playExplosion = function(position){

              var animations = {
                idle: [{
                  x: 15,
                  y: 16,
                  width: 35,
                  height: 35
                }, {
                  x: 76,
                  y: 13,
                  width: 42,
                  height: 39
                }, {
                  x: 137,
                  y: 10,
                  width: 48,
                  height: 45
                }, {
                  x: 199,
                  y: 9,
                  width: 52,
                  height: 48
                }, {
                  x: 262,
                  y: 7,
                  width: 55,
                  height: 52
                }, {
                  x: 4,
                  y: 70,
                  width: 59,
                  height: 54
                }, {
                  x: 68,
                  y: 69,
                  width: 60,
                  height: 56
                }, {
                  x: 131,
                  y: 69,
                  width: 61,
                  height: 56
                }, {
                  x: 2,
                  y: 133,
                  width: 62,
                  height: 57
                }, {
                  x: 66,
                  y: 132,
                  width: 62,
                  height: 58
                }, {
                  x: 130,
                  y: 132,
                  width: 62,
                  height: 58
                }, {
                  x: 194,
                  y: 132,
                  width: 62,
                  height: 58
                }, {
                  x: 258,
                  y: 132,
                  width: 62,
                  height: 58
                }, {
                  x: 2,
                  y: 196,
                  width: 62,
                  height: 58
                }, {
                  x: 66,
                  y: 196,
                  width: 62,
                  height: 58
                }, {
                  x: 132,
                  y: 196,
                  width: 59,
                  height: 58
                }, {
                  x: 198,
                  y: 197,
                  width: 57,
                  height: 57
                }, {
                  x: 262,
                  y: 197,
                  width: 56,
                  height: 57
                }, {
                  x: 9,
                  y: 264,
                  width: 46,
                  height: 53
                }, {
                  x: 77,
                  y: 267,
                  width: 40,
                  height: 45
                }, {
                  x: 148,
                  y: 278,
                  width: 24,
                  height: 28
                }]
              };
              that = this;
              var imageObj = new Image();
              imageObj.onload = function() {
                var blob = new Kinetic.Sprite({
                  x: position.X,
                  y: position.Y,
                  image: imageObj,
                  animation: 'idle',
                  animations: animations,
                  frameRate: 7,
                  index: 0
                });

        // add the shape to the layer
        that.layer.add(blob);
        // add the layer to the stage
        that.stage.add(layer);

        // start sprite animation
        blob.start();
      };
      imageObj.src = 'http://www.unicornmax.com/wp-content/uploads/2012/07/explosion-sprite-sheet-i0.png';

    }

    this.clear = function(){

      this.stage = new Kinetic.Stage({
        container: 'kontainer',
        width: 800,
        height: 600
      });

      this.bgLayer = new Kinetic.Layer();

      this.layer = new Kinetic.Layer();
    }

    this.update_frame = function(room){

    // check collision here with room.elementsCollision
    if(room.collisionElements != undefined && room.collisionElements.length > 0){
      for (var i = room.collisionElements.length - 1; i >= 0; i--) {
            // pour chaque pair d'objet collided
            // check si collision avec mur ou ememy
            if (room.collisionElements[i].objA != 'WALL' && room.collisionElements[i].objA != undefined){
              this.destroyImage(room.collisionElements[i].objA);
              coords = (room.collisionElements[i].objA.ship == undefined ? {X : room.collisionElements[i].objA.X, Y :  room.collisionElements[i].objA.Y} : {X : room.collisionElements[i].objA.ship.coords.X, Y :  room.collisionElements[i].objA.ship.coords.Y} );
              //this.playExplosion(coords);
            }
            if (room.collisionElements[i].objB != 'WALL' && room.collisionElements[i].objB != undefined){
              this.destroyImage(room.collisionElements[i].objB);
              coords = (room.collisionElements[i].objB.ship == undefined ? {X : room.collisionElements[i].objB.X, Y : room.collisionElements[i].objB.Y} : {X : room.collisionElements[i].objB.ship.coords.X, Y : room.collisionElements[i].objB.ship.coords.Y} );
              //this.playExplosion(coords);
            }
          };
        }

        for (var i = room.players.length - 1; i >= 0; i--) {

          player = room.players[i];
          this.moveImage(player.ship.id, player.ship.coords.X, player.ship.coords.Y, this.layer);
        };
        this.displayWaves(room.waves);

        this.displayShoots(room.shoots);
        console.log(room.shoots);
      // redraw layers to display ui            
      gameUi.stage.draw();
      gameUi.layer.draw();

    }
    // end class gameUi
  }

  var gameUi = new gameUi();
