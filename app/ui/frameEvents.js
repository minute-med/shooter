$(document).ready(function(){

	socket.on('destroy-ship-ui',function(socketid){
		gameUi.destroyImageById(socketid);
		gameUi.layer.clear();
		console.log('destroy leaver ship shape');
	});

	socket.on('end-game',function(status){
		socket.emit('client-pause-game');
		if(status == 'win') {
			selector = '#win_game_modal';
		} else if(status == 'loose') {
			selector = '#loose_game_modal';
		} else {
			selector = '#loose_game_modal';
		}
		$(selector).modal({
			backdrop : false,
			keyboard : false
		});
	});

	socket.on('start_game',function(room){
		$('#room').html('');
		$('#room_menu').hide();
		$('#kontainer').show();
		gameUi.init(room);

		$(window).keydown(function(evt) {

			if ($('#kontainer').css('display') != 'block')
				return;
			evt.preventDefault();
			/*******************************

			32 espace 	16 shift
			17 ctrl 	18 alt
			38 haut 	37 gauche
			39 droite 	40 bas

			*******************************/

			switch(evt.which)
			{

				case 17:
				socket.emit('loadShoot');
				console.log('pre-shooting');
				break;

			// left move, emit move event to server
			case 37:
			socket.emit('left-move-start');
			break;

			// up move, emit move event to server
			case 38:
			socket.emit('up-move-start');
			break;
			
			// right move, emit move event to server
			case 39:
			socket.emit('right-move-start');
			break;
			
			// down move, emit move event to server
			case 40:
			socket.emit('down-move-start');
			break;

			default:
			console.log('default key down handler');
		}
		//keydown event
	});


		$(window).keyup(function(evt) {

			if ($('#kontainer').css('display') != 'block')
				return;
			evt.preventDefault();
			/*******************************

			32 espace 	16 shift
			17 ctrl 	18 alt
			38 haut 	37 gauche
			39 droite 	40 bas
			27 echap
			*******************************/
			switch(evt.which)
			{

				// menu pause game
				case 27:
				socket.emit('client-pause-game');
				$('#pause_menu_modal').modal({
					backdrop : false,
					keyboard : false
				});
				break;

				case 17:
				socket.emit('releaseShoot');
				break;


				// left move, emit move event to server
				case 37:
				socket.emit('left-move-stop');
				break;

				// up move, emit move event to server
				case 38:
				socket.emit('up-move-stop');
				break;

				// right move, emit move event to server
				case 39:
				socket.emit('right-move-stop');
				break;

				// down move, emit move event to server
				case 40:
				socket.emit('down-move-stop');
				break;

				default:
				console.log('default key up handler');
				console.log(evt.which);
			}
			//keyup event
		});
});

});