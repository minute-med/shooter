$(document).ready(function(){

// une game a ete creer 
// recuperer la liste des rooms et l'afficher dans la salle multi
socket.on('update_multi_rooms',function(rooms){
	$('#rooms').html('');
	roomManager.updateMultiRooms(rooms);
});



// multiplayer menu's create room button click handler
	$('#create_room').submit(function(evt){
		evt.preventDefault();

		room_name = $('#room_name').val();
		room_info = {
			room_name:room_name,
			room_owner:'player1'
		};

		socket.emit('create_room', room_info,function(room){
			$('#room').html('');
			if (typeof $('#cur_game_name').length != 'undefined'){
				$('#cur_game_name').remove();
			}
			$('#room_menu').append('<p id="cur_game_name">game name: ' + room.roomName +  '</p>');
			$('#room').append('<div>' + room.Owner +  '</div>');

		});
		
		$(this).parent().hide();
		$('#room_menu').show();
	});


	/************************************************
	
	when user joined my game room i update the UI
	replace by roomManager method roomManager.updateGameRoom(room);
	*************************************************/
	

	socket.on('update_game_room',function(room){
		$('#room').html('');
		for (var i = room.players.length - 1; i >= 0; i--) {
			$('#room').append('<div>' + room.players[i].nickname +  '</div>');
		};
	});


	/************************************************
	
	when player click on game room, we emit 'join_room' event,
	
	then in callback, we update UI to display the game room
	
	with the room infos received from server  

	*************************************************/
	$(document).on('click','.join_room',function(){

		room_name = $(this).attr('id').trim().split('_').join(' ');

		socket.emit('join_room', room_name, function(room){
			// update de l'UI (fenetre) apres avoir join la room
			// a remplacer par une fonction
			$('#multi_menu').hide();
			$('#room').html('');
			$('#room_menu').append('<p id="cur_game_name" >game name: ' + room.roomName +  '</p>');
			for (var i = room.players.length - 1; i >= 0; i--) {
				$('#room').append('<div>' + room.players[i].nickname +  '</div>');
			};
			$('#room_menu').show();
		});

	});

});