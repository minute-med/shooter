$(document).ready(function(){

	function request_nickname(message){
		//message = (message != undefined ? message : message  + ' enter a username');
		var nick = '';
		do{
			nick = prompt(message);
		}
		while(nick == '' || nick == null);
		return (nick);
	}

	$('.back').click(function(){
		var parent = $(this).parent();
		parent_menu_selector = '#' + $(this).data('parent');
		parent.hide();
		$(parent_menu_selector).show();
		// si je suis dans une room, et que je suis l'owner
		// la room est supprime
	});

	socket.on('request_nickname',function(previous_error){
		message = 'veuillez entrer un pseudo';
		var nickname = request_nickname(message);
		socket.emit('response_nickname', nickname);
	});

	socket.on('user_already_exist',function(nickname){
		message = 'le pseudo : ' + nickname + ' existe deja, entrer un autre pseudo';
		var nickname = request_nickname(message);
		socket.emit('response_nickname', nickname);
	});
	

	socket.on('goto-main_menu',function(){
		gameUi.clear();
		$('#kontainer').hide();
		$('#main_menu').show();
	});

	$(document).on('click','#main_menu button',function(){
		var targetSelector = '#' + $(this).val() + '_menu';
		// emission de l'event 'je rentre dans la page multijoueurs'
		// j'update les games disponible et les affiche
		if($(this).val() == 'multi') {
			socket.emit('join_multi_room',{hi:'hi'},function(rooms){
				roomManager.updateMultiRooms(rooms);
			});
		}
		else if ($(this).val() == 'options'){
		}
		$('#container').children().hide();
		$('#main_menu').hide();

		$(targetSelector).show();

		//affichage du nouveau menu
	});


	
/********************************************


launch game event

*********************************************/
$('#launch_game').click(function(evt){
	
	map_size = {
		w:	$('#kontainer').width(),
		h: 	$('#kontainer').height()
	};

	socket.emit('launch_game', map_size);
});

socket.on('update_frame',function(room){
	gameUi.update_frame(room);
});
	// lauch_game event (socket)

/******************************

	ingame menu options

	******************************/

	$('.unpause-game').click(function(evt){
		socket.emit('client-unpause-game');
	});

	$('.quit-game').click(function(evt){
		socket.emit('quit-game');
		gameUi.clear();

	});



/******************************

options
******************************/

$( "#music_option" ).val("off");

$('#music_option').click(function(){
	val = ($(this).val() == 'on' ? 'off' : 'on' );
	
	music = document.getElementById('music');
	if(val == 'off') {
		music.pause();
		$(this).val(val);
	} else {
		music.play();
		$(this).val(val);
	}
});




});	