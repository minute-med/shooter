var http = require('http');
var fs = require('fs');

var Game 		= require('./app/classes/server/Game.js');
var GameManager		= require('./app/classes/server/GameManager.js');
var Room 		= require('./app/classes/server/Room.js');
var RoomManager		= require('./app/classes/server/RoomManager.js');
var User		= require('./app/classes/server/User.js');
var UserManager		= require('./app/classes/server/UserManager.js');

var server = http.createServer(function (req, res){

	req.on('end',function (){
		
		res.writeHead(200,{'Content-Type':'text/html'});
		res.end();
	});

}).listen(8081);


var io = require('socket.io').listen(server);

var userManager = new UserManager(io.sockets);
var roomManager = new RoomManager(io.sockets);
var gameManager = new GameManager(io.sockets);

io.sockets.on('connection',function(socket){

/*********************************************

on connection, ask for nickname, 
creat a user with the userManager

*********************************************/

socket.emit('request_nickname');

socket.on('response_nickname',function(nickname){

	if (userManager.userExist(nickname)) {
		socket.emit('user_already_exist', nickname);
		return;
	}
	user = new User(nickname, socket);
	userManager.addUser(user);
});

socket.on('create_room', function(room_info,fn){
	/*
		NEW STUFF
		*/

		user = userManager.getUserById(socket.id);
		if(user){
			room = roomManager.createRoom(socket, room_info.room_name, user);
			console.log(room);
			fn(room);
			socket.broadcast.emit('update_multi_rooms', roomManager.rooms);
		} else {
			console.log("error user not exist : \n\n");
		}
	});

/********************************************


a l'event 'join_multi_room'(connection a la page multijoueurs)

je renvoie les rooms disponible pour que le client les affiche

*********************************************/
socket.on('join_multi_room',function(hi,fn){
	fn(roomManager.rooms);
});

/********************************************

a l'event 'join_room'(connection a une game room)
renvoi les infos sur la room rejoint

*********************************************/
socket.on('join_room', function(room_name, fn){

	user = userManager.getUserById(socket.id);

	if(user){
		room = roomManager.joinRoom(socket, room_name, user);
		fn(room);
	}

	/* 
	sending event to the clients of the joined room to refresh their UI
	*/
	socket.broadcast.to(room_name).emit('update_game_room', room);

});

/********************************************

launch game event


*********************************************/


socket.on('launch_game',function(mapSize){

	room = roomManager.getRoomByUserId(socket.id);

	path = 'app/config/wavesTemplate.json';
	wavestemplate = undefined;
	fs.readFile(path, function (err, data) {
		if (err) { 
			throw err;
			return false;
		}
		var wavestemplate = JSON.parse(data);
	});

	game = gameManager.createGame(room, mapSize, wavestemplate);

	socket.emit('start_game', game.room);

	game.init(io.sockets);

	game.run(io.sockets);

	roomManager.removeRoomByName(game.room.roomName);

	socket.broadcast.emit('update_multi_rooms', roomManager.rooms);


});

socket.on('quit-game',function(){

	/*- enlever le jouer de la game*/
	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false){

		socket.emit('goto-main_menu');	
		console.log("\n\n\n\n\n\n\n\n\n\n\n\n");		
		console.log("quit game returned false\n gameManager.games[] : \n");		
		console.log(gameManager.games);
		console.log("\n\n\n\n\n\n\n\n\n\n\n\n");
		return false;
	}

	game.room.removePlayer(socket.id);
	game.updateKeyToId(socket.id);
	socket.leave(game.room.roomName);


	/*si game vide, supprimer la game*/
	if(game.room.players.length < 1){
		gameManager.removeGame(game.room.roomName);
		delete game;
	}
	else {
		if(game.status == 'pause'){
			//socket.broadcast.to()'destroy-ship-ui', socket.id);
game.run(io.sockets);
}
}

	/*
	envoyer un event au client pour 
	renvoyer le jouer au menu principal
	*/
	socket.emit('goto-main_menu');	
	/*
	envoyer un event a 'tout' les clients pour remove 
	le vaisseau de la map ?
	*/
	//socket.broadcast.to(game.room.roomName).emit('destroy-leaved-player', socket.id)
});

socket.on('client-pause-game',function(){
	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	game.pause();
});

socket.on('client-unpause-game',function(){
	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	game.unpause(io.sockets);	
});

// start move left
socket.on('left-move-start',function(){

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	key  = game.KeyToId[socket.id];
	
	game.room.players[key].ship.speed.Left = true;
	game.room.players[key].ship.speed.Right = false;

});

// stop move left
socket.on('left-move-stop',function(){

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	key = game.KeyToId[socket.id];

	game.room.players[key].ship.speed.Left = false;
	
});

// start move up
socket.on('up-move-start',function(){
	
	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	playerKey = game.KeyToId[socket.id];
	
	game.room.players[playerKey].ship.speed.Up = true; 
	game.room.players[playerKey].ship.speed.Down = false; 

});


// stop move up
socket.on('up-move-stop',function(){
	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	playerKey = game.KeyToId[socket.id];
	
	game.room.players[playerKey].ship.speed.Up = false; 
});

// start move right
socket.on('right-move-start',function(){

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	playerKey = game.KeyToId[socket.id];
	game.room.players[playerKey].ship.speed.Right = true; 
	game.room.players[playerKey].ship.speed.Left = false; 
	
});

// stop move right
socket.on('right-move-stop',function(){

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	playerKey = game.KeyToId[socket.id];
	game.room.players[playerKey].ship.speed.Right = false; 
});

// start move down
socket.on('down-move-start',function(){

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	playerKey = game.KeyToId[socket.id];

	game.room.players[playerKey].ship.speed.Down 	= true;
	game.room.players[playerKey].ship.speed.Up 	= false;
});

// stop move down
socket.on('down-move-stop', function(){

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	playerKey = game.KeyToId[socket.id];

	game.room.players[playerKey].ship.speed.Down 	= false;
});

// shooting
socket.on('loadShoot', function(){

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	playerKey = game.KeyToId[socket.id];

	game.room.players[playerKey].shootLoad++;
	// le player tire, il faut augmenter le shootLoad

});

socket.on('releaseShoot', function(){

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false)
		return false;
	playerKey = game.KeyToId[socket.id];
	
	if (game.room.players[playerKey].shootLoad >= 75) {
		// shootLoad est >= 150, et je cree un gros tir,
		game.createBigShoot(game.room.players[playerKey]);
		game.room.players[playerKey].shootLoad = 0;
	} else {
		// il est inferieur a 150(valeur pour gros tir),et je cree un tir normal (je vide le shootLoad)
		game.createShoot(game.room.players[playerKey]);
		game.room.players[playerKey].shootLoad = 0;
	}

});



socket.on('disconnect', function () {

	// checker si le jouer est en game,
	// si oui, et que la game est vide, supprimer la game et la room
	
	deleted_user = userManager.removeUserById(socket.id);
	console.log('deleted user : ' + deleted_user.nickname);
	delete deleted_user;

	game = gameManager.getGameByUserId(socket.id);
	if(game == undefined || game == false) {
		return;
	} else {
		game.pause();
		game.room.removePlayer(socket.id);
		socket.leave(game.room.roomName);		

		/*si game vide, supprimer la game*/
		if(game.room.players.length < 1) {
			gameManager.removeGame(game.room.roomName);
			delete game;
			return;
		}
		else {
			game.updateKeyToId(socket.id);
			//socket.broadcast.to()'destroy-ship-ui', socket.id);
game.run(io.sockets);

}


}
});

});
